{
  nixConfig.bash-prompt = "[nix-develop-spooky:] ";
  description = "A very basic flake";
  inputs.haskellNix.url = "github:input-output-hk/haskell.nix";
  inputs.nixpkgs.follows = "haskellNix/nixpkgs-unstable";
  inputs.flake-utils.url = "github:numtide/flake-utils";
  outputs = { self, nixpkgs, flake-utils, haskellNix }:
    flake-utils.lib.eachSystem [ "x86_64-linux" ] (system:
      let
        deferPluginErrors = true;
        overlays = isTyped: [
          haskellNix.overlay
          (final: prev: {
            # This overlay adds our project to pkgs
            haskellOverrides = hself: hsuper: {
              mkDerivation = args: hsuper.mkDerivation (args //
                (if isTyped then { configureFlags = "-f typed"; } else {}));
            };
            spooky = final.haskell-nix.project'
              { src = ./.;
                compiler-nix-name = "ghc8107";
                projectFileName = "stack.yaml";
                shell.tools = {
                  cabal = { };
                  ghcid = { };
                  hlint = { };
                  haskell-language-server = { };
                  stylish-haskell = { };
                };
                # Non-Haskell shell tools go here
                shell.buildInputs = with pkgs; [
                  nixpkgs-fmt
                ];
              };
          })
        ];
        pkgs = import nixpkgs { inherit system; overlays = (overlays false); inherit (haskellNix) config; };
        pkgs-typed = import nixpkgs { inherit system; overlays = (overlays true); inherit (haskellNix) config; };
        flake-untyped = pkgs.spooky.flake { };
        flake-typed   = pkgs-typed.spooky.flake { };
      in
      flake-untyped // {
        packages =
            (pkgs.lib.mapAttrs' (name: value: { name = "${name}-typed"; inherit value; }) flake-typed.packages)
         // (pkgs.lib.mapAttrs' (name: value: { name = "${name}-untyped"; inherit value; }) flake-untyped.packages);
        defaultPackage = flake-untyped.packages."spooky:lib:spooky";
        devShell = flake-untyped.devShell.overrideAttrs (oa: {
          shellHook = oa.shellHook + ''
            alias haskell-language-server-wrapper=haskell-language-server
            ${pkgs.lolcat}/bin/lolcat ${./intro}
          '';
        });
      });
}
